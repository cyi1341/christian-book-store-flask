# Importing necessary modules
from functools import wraps
from flask import Flask, render_template, redirect, url_for, flash, request, abort
from flask_login import LoginManager, login_user, logout_user, login_required, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo
import pymysql, os
from datetime import datetime

# Creating a login form with username and password fields
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Log In')

# Creating a registration form with username, email, password, and confirm password fields
class RegisterForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('confirm_password', message='Passwords must match')])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired()])
    submit = SubmitField('Register')


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret-key'

# Creating a login manager and initializing it with the Flask app
login_manager = LoginManager()
login_manager.init_app(app)

# Defining a function to load a user based on their user ID
@login_manager.user_loader
def load_user(user_id):
    user = User.get(int(user_id))
    if not user:
        user = Admin.get(int(user_id))
    return user

# Function to establish a connection to the database
def get_db_connection():
    return pymysql.connect(
        host='db4free.net',
        user='cyi1341',
        password=os.environ.get('MYSQL_PASSWORD'),
        database='cyi1341',
        port=3306
    )

# Creating a UserMixin class with methods for validating login and getting user information
class UserMixin:
    def __init__(self, id, username, email, password):
        self.id = id
        self.username = username
        self.email = email
        self.password = password

    @classmethod
    def validate_login(cls, username, password):
        conn = get_db_connection()
        with conn:
            with conn.cursor() as cursor:
                cursor.execute('SELECT * FROM {} WHERE username=%s'.format(cls.table_name), (username,))
                result = cursor.fetchone()
                if result:
                    user = cls(*result)
                    if password == user.password:
                        return user
        return None

# Creating a User class that inherits from UserMixin and has additional methods for checking user status and getting user information
class User(UserMixin):
    table_name = 'users'

    def __init__(self, id, username, email, password):
        super().__init__(id, username, email, password)
        self.active = True

    def is_active(self):
        return self.active

    def get_id(self):
        return str(self.id)

    def is_authenticated(self):
        return True

    @classmethod
    def get(cls, user_id):
        conn = get_db_connection()
        with conn:
            with conn.cursor() as cursor:
                cursor.execute('SELECT * FROM {} WHERE id=%s'.format(cls.table_name), (user_id,))
                result = cursor.fetchone()
                if result:
                    return cls(*result)
        return None


# Creating a Book class with methods for getting all books and getting a single book by ID
class Book:
    def __init__(self, id, title, author, description):
        self.id = id
        self.title = title
        self.author = author
        self.description = description
        self.reviews = []

    @staticmethod
    def get_all():
        conn = get_db_connection()
        with conn:
            with conn.cursor() as cursor:
                cursor.execute('SELECT books.id, books.title, books.author, books.description, reviews.reviewer_name, reviews.review_content, reviews.star_rating FROM books LEFT JOIN reviews ON books.id = reviews.book_id')
                results = cursor.fetchall()
                books = {}
                for result in results:
                    if result[0] not in books:
                        book = Book(result[0], result[1], result[2], result[3])
                        books[result[0]] = book
                    if result[4]:
                        review = {'reviewer_name': result[4], 'review_content': result[5], 'star_rating': result[6]}
                        books[result[0]].reviews.append(review)
                return list(books.values())

    @staticmethod
    def get_one(book_id):
        conn = get_db_connection()
        with conn:
            with conn.cursor() as cursor:
                cursor.execute('SELECT id, title, author, description FROM books WHERE id = %s', (book_id,))
                result = cursor.fetchone()
                if result:
                    return Book(result[0], result[1], result[2], result[3])
                else:
                    return None

# Creating routes for the Flask app
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
    # Creating a registration form
    form = RegisterForm()
    if form.validate_on_submit():
        conn = get_db_connection()
        with conn:
            with conn.cursor() as cursor:
                cursor.execute('SELECT * FROM users WHERE username=%s', (form.username.data,))
                result = cursor.fetchone()
                if result:
                    flash('Username already taken, please choose a different one', 'error')
                else:
                    cursor.execute('INSERT INTO users (username, email, password) VALUES (%s, %s, %s)',
                                    (form.username.data, form.email.data, form.password.data))
                    conn.commit()
                    flash('Registration successful, please login', 'success')
                    return redirect(url_for('login'))
    return render_template('register.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    # Redirecting to dashboard if user is already logged in
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))

    # Handling login form submission
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        remember = request.form.get('remember')
        user = User.validate_login(username, password)

        if user:
            login_user(user, remember=remember)
            next_page = request.args.get('next')
            return redirect(next_page or url_for('dashboard'))

        flash('Invalid username/password combination')
        return redirect(url_for('login'))

    return render_template('login.html')

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been logged out', 'success')
    return redirect(url_for('index'))


@app.route('/books', methods=['GET'])
@login_required
def books():
    books = Book.get_all()
    return render_template('books.html', books=books)

@app.route('/books/<int:book_id>', methods=['GET', 'POST'])
@login_required
def book_details(book_id):
    # Get the book with the specified ID from the database
    book = Book.get_one(book_id)
    # If the book doesn't exist, return a 404 error
    if not book:
        abort(404, description="Book not found")

    # If the request method is POST, add a new review for the book
    if request.method == 'POST':
        # Get the review content, star rating, and reviewer name from the form data
        review_content = request.form['review_content']
        star_rating = request.form['star_rating']
        reviewer_name = request.form['reviewer_name']
        # Connect to the database
        conn = get_db_connection()
        with conn.cursor() as cursor:
            # Check if the user has already submitted a review for this book
            cursor.execute('SELECT id FROM reviews WHERE book_id = %s AND id = %s', (book_id, current_user.id))
            existing_review = cursor.fetchone()
            # If the user has already submitted a review, delete the old review
            if existing_review:
                cursor.execute('DELETE FROM reviews WHERE book_id = %s AND id = %s', (book_id, current_user.id))
            # Insert the new review into the database
            cursor.execute('INSERT INTO reviews (id, book_id, reviewer_name, review_content, star_rating) VALUES (%s, %s, %s, %s, %s)', (current_user.id, book_id, reviewer_name, review_content, star_rating))
            conn.commit()

    # Connect to the database
    conn = get_db_connection()
    with conn.cursor() as cursor:
        # Get all the reviews for the book with the specified ID from the database
        cursor.execute('SELECT reviewer_name, review_content, star_rating FROM (SELECT *, ROW_NUMBER() OVER () AS row_num FROM reviews WHERE book_id = %s) sub ORDER BY row_num DESC', (book_id,))
        reviews = cursor.fetchall()

    # Render the book details page with the book and its reviews
    return render_template('book_details.html', book=book, reviews=reviews)


@app.route('/books/<int:book_id>/forum', methods=['GET', 'POST'])
@login_required
def book_forum(book_id):
    conn = get_db_connection()
    cursor = conn.cursor()

    # If the request method is POST, check if the user wants to delete a comment or add a new one.
    if request.method == 'POST':
        # If the user wants to delete a comment, delete it from the database.
        if 'delete_comment' in request.form:
            # Get the comment ID and user ID of the comment to be deleted.
            comment_id = int(request.form['delete_comment'])
            user_id = current_user.id

            # Check if the user is authorized to delete the comment.
            cursor.execute('SELECT user_id FROM forum WHERE id = %s', (comment_id,))
            comment_user_id = cursor.fetchone()[0]

            # If the user is authorized, delete the comment and commit the changes to the database.
            if user_id == comment_user_id:
                cursor.execute('DELETE FROM forum WHERE id = %s', (comment_id,))
                conn.commit()
                flash('Comment deleted successfully!', 'success')
            # If the user is not authorized, show an error message.
            else:
                flash('You are not authorized to delete this comment!', 'danger')

            # Redirect the user back to the book forum page.
            return redirect(url_for('book_forum', book_id=book_id))

        # If the user wants to add a new comment, insert it into the database.
        else:
            # Get the comment, user ID, and current date and time.
            comment = request.form['comment']
            user_id = current_user.id
            created_at = datetime.now()

            # Insert the comment into the database and commit the changes.
            cursor.execute('INSERT INTO forum (comment, user_id, created_at, book_id) VALUES (%s, %s, %s, %s)', (comment, user_id, created_at, book_id))
            conn.commit()

            # Show a success message and redirect the user back to the book forum page.
            flash('Comment posted successfully!', 'success')
            return redirect(url_for('book_forum', book_id=book_id))

    # Get all the comments for the book from the database, along with the username of the commenter and the comment creation date.
    cursor.execute('SELECT c.id, c.comment, c.created_at, u.username, c.user_id FROM forum c JOIN users u ON c.user_id = u.id WHERE c.book_id = %s ORDER BY c.created_at DESC', (book_id,))
    comments = cursor.fetchall()

    # Get the title of the book from the database.
    cursor.execute('SELECT title FROM books WHERE id = %s', (book_id,))
    book_title = cursor.fetchone()[0]

    # Render the book forum page with the comments and book title.
    return render_template('book_forum.html', comments=comments, book_title=book_title)


@app.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dashboard():
    # Get the user ID of the logged-in user.
    user_id = current_user.id
    
    # Establish a connection to the database.
    conn = get_db_connection()

    # If the request method is POST, check if the user wants to delete a book review or a forum comment.
    if request.method == 'POST':
        # If the user wants to delete a book review, delete it from the database.
        book_id = request.form.get('book_id')
        comment_id = request.form.get('comment_id')

        if book_id:
            with conn.cursor() as cursor:
                cursor.execute('DELETE FROM reviews WHERE book_id = %s AND id = %s', (book_id, user_id))
            conn.commit()

        # If the user wants to delete a forum comment, delete it from the database.
        if comment_id:
            with conn.cursor() as cursor:
                cursor.execute('DELETE FROM forum WHERE id = %s AND user_id = %s', (comment_id, user_id))
            conn.commit()

    # Get all the book reviews and forum comments for the user from the database.
    with conn.cursor() as cursor:
        cursor.execute('SELECT reviews.book_id, reviews.reviewer_name, books.title, reviews.review_content FROM reviews INNER JOIN books ON reviews.book_id = books.id WHERE reviews.id = %s', (user_id,))
        reviews = cursor.fetchall()
        cursor.execute('SELECT forum.id, forum.comment, forum.created_at, books.title FROM forum INNER JOIN books ON forum.book_id = books.id WHERE forum.user_id = %s', (user_id,))
        comments = cursor.fetchall()

    # Close the database connection and render the dashboard page with the user's book reviews and forum comments.
    conn.close()
    return render_template('dashboard.html', reviews=reviews, comments=comments)


@app.route('/about')
def about():
    return render_template('about.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


